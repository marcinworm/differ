const fse = require("fs-extra");
const puppeteer = require("puppeteer");
const config = require("./config.json");

const dir = __dirname + "/screenshots/";
!fse.existsSync(dir) ? fse.mkdirsSync(dir) : fse.removeSync(dir);

async function setViewports(device, url, env) {
    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    await page.waitFor(1000);
    await page.goto(url.link, {waitUntil: "networkidle2"});

    await page.setViewport({
        width : device.width,
        height: device.height
    });
    await getScreenshots(device, url, page, browser, env);
}

async function getScreenshots(device, url, page, browser , env) {
    var new_location = dir + "/" + device.name+`(`+ device.width+`-`+device.height +`)`;
    fse.mkdir(new_location, function (err) {
        if (err) {
            console.log(err)
        }
    });

    await page.screenshot({
        path: new_location +`/`+ url.name + `.png`,
        fullPage: true
    });
    browser.close();
}

async function getUrlAndResolutions(devices, urls, env) {
    for (let device of devices) {
        for (let url of urls) {
            await setViewports(device, url, env);
        }
    }
}

getUrlAndResolutions(config.devices, config.routes.prod, "prod");


// const { imgDiff } = require("img-diff-js");

// imgDiff({
//   actualFilename: "screenshots/1.png",
//   expectedFilename: "screenshots/2.png",
//   diffFilename: "screenshots/diff.png",
// }).then(result => console.log(result));